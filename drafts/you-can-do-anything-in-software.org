#+TITLE: You can do anything in software
#+DATE: <2022-07-13 Wed>

#+begin_quote
You can do anything in software
#+end_quote

It's a quote my former colleague dropped every now and then. And he's right. I
remember one time we had to come up with a pretty creative solution, and I'm
still proud at the workaround we found.

** A tale of touchscreens
:PROPERTIES:
:CUSTOM_ID: tale-of-touchscreens
:END:

In a previous job, I was working in a team building software for an embedded
GNU/Linux device with a touchscreen. And at one point we needed to add support
for a different type of touchscreen. No big deal, you think.

The old touch screen connected over a 9600 baud RS232 connection and constantly
sent out 17 bytes of data, one byte for each of the 8 horizontal and 8 vertical
sensor wires and a zero-terminator byte. The code in the UI application was
constantly reading these bytes from ~/dev/ttyS1~. The data was really raw and
therefore the application needed to do complex floating point arithmetic to
process it. To detect a touch and to calculate the location it normalized the
signals, then performed a 5th degree polynomial, and applied compensation for
the cut-off at the sides.

This code was written before I was working there, but my colleagues told me more
than once they had spent a ton of time making this algorithm robust. Because the
device was installed on a gas station, there was a lot of interference from
objects nearby. True story, in early versions people had their bank card blocked
because during PIN entry a heavy truck pulled up next to them and the screen
suddenly was detecting false touches from that. With this knowledge, we expected
quite some trouble with the new touchscreen as well.

The new touch screen connected over I2C. With I2C you basically have a address
space you can read from at any time and it would give you the coordinates of the
touch (or touches, because it supported multi-touch). It also had an interrupt
pin, so we could detect touches event-driven, rather than on a polling basis.
During testing it seemed to be very reliable, great!

We wrote a simple GNU/Linux driver that added a character device ~/dev/touch~ to
user-space. User-space applications could ~read()~ from it to get the touches
and coordinates, and we also implemented the ~poll~ file operation so the app
could [[https://linux.die.net/man/2/select][~select()~]] to wait for interrupts.

Okay, so we just need to change the UI application to read from that device when
a new type of touch screen is detected. Unfortunately it was not that easy…

** The release cycle
:PROPERTIES:
:CUSTOM_ID: release-cycle
:END:

(sorry, this is a boring part to explain why we had to be creative with a
solution, if you like you can skip to [[enter-the-os-layer][Enter the OS layer]])

The device I'm talking about connects with a [[https://en.wikipedia.org/wiki/Point_of_sale][POS]], using a proprietary [[https://en.wikipedia.org/wiki/Remote_procedure_call][RPC]]
protocol. Because the RPC changed almost every major release, our major version
numbers were tightly coupled (~<our-major-version> + 17 =
<their-major-version>~). When a new payment terminal was installed at the
station, the POS would force software installation of the latest major version
that's compatible with the POS. So this could also result in a downgrade.

We did roughly 4 major releases a year, and because our business model required
the customer to pay for a major version upgrade, we had *a lot* different
versions running in the field.

This combined would make it really





We provided maintenance support for bug fixes 4 on
the last 3 versions and LTS version.

Building release packages was pretty tedious:

+ Build software packages for our device
+ Get them approved and signed by a security officer
+ Deliver that to the POS team to package in their release

Also getting it deployed at the customer's site was not easy. At the time we did
not have any tools to deploy software updates over the internet, so updates
usually only happened when a service technician was there to install the package
from DVD.

So backporting the changes for the UI application was not option.

** Enter the OS layer
:PROPERTIES:
:CUSTOM_ID: enter-the-os-layer
:END:

Luckily we were in full control of the software stack on this device. The UI
application I spoke about earlier was booted by an installer application. This
installer was part of the linux-based OS and did not have the incompatibility
issue with the POS.

This is where had to get creative, we needed to find a solution so the old UI
application could work with the new touch screen. And we came up with an
emulation layer. A piece of software should translate the touches and
coordinates back to the 16 raw wire signals the UI application expected.

** Floating points

Great, we knew the math for this, we only had to reverse it. But here we had to
get creative a second time. The device driver for the touch screen was written
as a GNU/Linux driver, and floating point arithmetic in the kernel is …
unexisting.

Here my colleague came up with a brilliant solution: let's pre-calculate all the
wire levels. The screen was 640 by 480 pixels, and there were 8 wires per
axis. We had two two-dimensional arrays, one for the vertical wires, and one for
the horizontal wires. Storage-wise this would mean 640 times 8 byte for the
vertical wires, and 480 ✕ 8 bytes for the horizontal wires. That's a total of
8960 bytes.

With this in place, simulating touches was really fast. In pseudo-code it would
be something like:

#+begin_src c
#include <string.h>

unsigned char vertical[][8] = {
  { 0xff, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00 },
  { 0xef, 0x0f, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00 },
  // ...
};

unsigned char horizontal[][8] = {
  { 0xff, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00 },
  { 0xef, 0x0f, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00 },
  // ...
};

void touch_to_wires(short x, short y, unsigned char *bytes) {
  memcpy(bytes, horizontal[x], 8);
  memcpy(bytes + 8, vertical[y], 8);
}
#+end_src

This was fast, simple and we even could tune the values to counter for the
compensation happening on the side. We tested every coordinate against the
algorithm in the UI application, and perfected the values to get an exact match
on the expected coordinate.

** todo

When a device is manufactured, it gets loaded with the latest software. But to
ensure RPC compatibility, when the device is placed in the field, the POS forces
a software upload with the version compatible with the POS version. In most
cases this was a downgrade.

Of course we could backport the UI application changes, but it would involve
more than a dozen versions. And it would be a logical nightmare. Because we
released all software together, we had an installation package of
300-400MB. That does not sound big, but most installations only had a 56k
internet connection (or worse), enough to transmit payment transaction, but not
enough for software updates. So our service technicians carried DVDs for every
maintained software version. For /only/4 versions this was a real pain, so it
would be possible to do this for more versions.

Because








It would be a logistical nightmare.
